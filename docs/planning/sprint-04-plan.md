# Sprint 4 (Aug 26 - Sep 02)

> Main goal: complete the frontend
 
* [] R1: Use anchor elements to display the state triggers in the chat
* [] R1: Create clickable state triggers and advance to the following state
* [] R2: Search for some online chatbot examples 
* [] R2: Enhance `chameleon`'s appearance and make it familiar to a variety of users
* [] R2: Incorporate the useful features into the chat widget `chameleon`
* [] R2: Come up with your own features and add them to the widget
* [] R1: Add 'Chameleon is typing ...' before the message loads
* [] R2: Clean up the frontend code and make it understandble

## Backlog

* [] R2: Continue learning and practicing Django channels/Web Sockets in the [learning repo](https://gitlab.com/rochdikhalid/django-channels)
* [] R2: Learn the basics of Python Celery and how it works
* [] R2: Practice Python Celery in the learning repo
* [] R2: Fix the relevant bug that is causing the current frontend issue. 
* [] R?: Fix me: If user sends wrong response bot can't continue 
* [] R?: Fix me: Recognize variations on Espanol etc (nlp)
* [] R?: Fix me: Convert the has to a cryptogrpahic hash that includes a salt (key) for greater security
* [] Create release notes for 0.0.01 and 0.0.2
* [] R?: Fix me: [#5](https://gitlab.com/tangibleai/nudger/-/issues/5) Improve the Celery queueing / execution of messages
* [] Add a global trigger to reset conversation (copy/paste to all states - target: welcome state, reset - content dictionary)
* [] Create dialgoe for botfeedbac (add to existing YAML)
* [] Add security around eval function (Getattr instead)
* [] Be able to specify the variable name in the context dictionary from the YAML file (save whatever the user says into the variable in contenxt)
* [] Understand the sequence of events for logging/sending state (consumers.py > tasks.py).  Async the I/O bound tasks, and have them happen in the right order.

## Done: Sprint 3 (Aug 19 - Aug 26)

> Backend tasks (Poly - nudger)

* [x] R1-0.2: Install the dependencies and run locally the latest **main** branch of `nudger`
* [x] R0.5-0.1: Configure the Channel layers settings
* [x] R0.5-0.2: Setup Celery and Celery Beat in Django
* [x] R0.5-0.2: Setup Redis server
* [x] R1-0.7: Add Redis server as a task broker
* [x] R0.5-0.5: Create the needed environment variables to store the Django/Redis credentials
* [x] R1-0.5: Test manually Poly behaviors to find bugs that need to be solved

> Frontend tasks (Chameleon)

* [x] R1-0.5: Convert numerical room id to an alphanumeric hash
* [x] R0.5-0.1: Connect the Room component to [qary.ai](https://www.qary.ai/) or [staging.qary.ai](https://staging.qary.ai/)
* [x] R0.5-0.5: Pop up the welcome message from the Web Socket server
* [x] R1-1: Fix the welcome message display on the Room component
* [x] R0.5-0.2: Add Triggers (options) to the Room component as buttons 
* [x] R1-0.2: Identify Triggers in the Room component
* [x] R1-0.5: Maintain the state by storing/updating the current state
* [x] R0.5-0.2: Have the user responses appear in the chat
* [x] R1-0.5: Normalize the user inputs to handle different formats (lowercase, etc)
* [x] R1-0.5: Submit user inputs by hitting Enter
* [x] R1-0.7: Fix the web socket connection issue when the welcome popup is off
* [x] R0.5-0.5: Prevent the web socket to be called at "Resume the ..." button
* [x] R1-0.7: Prevent the web socket to be called when exit/back to the conversation
* [x] R1-0.5: Edit the source to call the Web Socket within the Door component


