# Sprint 20 (Aug 05 - Aug 12)

* [x] R0.2-R0.1: Clone the [svelte-tawk](https://github.com/hellpirat/svelte-tawk-to-chat-widget) repository
* [x] R1-1: Adapt what is useful from the [tawk tutorial](https://raw.githubusercontent.com/codebubb/tawk-tutorial/main/src/Tawk.js) repo 
* [x] H?: Create a new repo named `faceofqary` and make it public
* [x] R0.1-0.1: Clone the `faceofqary` repository
* [x] R0.2-0.1: Edit the README file by adding the project plan below
* [x] R0.2-0.1: Create an empty Svelte project called **client**
* [x] R0.2-0.1: Add a `.gitignore` file to ignore node_modules and build files
* [x] R0.2-0.1: Push the empty svelte project to the `faceofqary` repository
* [x] R3-R2.5: Build a chat widget component using Svelte
> 
* [x] R1-1: Create a chat-window component that contains an empty text box and chat header
* [x] R1-1: Add a **send** that sends messages
* [x] R0.5-0.5: Up-scroll previous messages in the same window
* [x] R0.5-0.5: Make the **chat conversation** button initialize the chat (welcome message)
* [x] R0.5-1: Make the chat bullet echo what the user says in the chat 
* [x] R0.5-0.5: Delay the welcome message
* [x] R0.5-0.5: Edit the **start conversation** button when the chat starts
>
* [x] R0.5-0.2: Create an empty django project in the same repository
* [x] R1-0.5: Structure the repository using the monorepo multi-server approach
* [x] R1-0.5: Create two web services called **faceofqary-front** and **faceofqary-back**
* [x] R1-1: Deploy the application on Render one step at a time

## Backlog

* [] R?: 