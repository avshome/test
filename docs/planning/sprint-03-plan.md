# Sprint 3 (Aug 19 - Aug 26)

> Backend tasks (Poly - nudger)

* [x] R1-0.2: Install the dependencies and run locally the latest **main** branch of `nudger`*
* [x] R0.5-0.1: Configure the Channel layers settings
* [x] R0.5-0.2: Setup Celery and Celery Beat in Django
* [x] R0.5-0.2: Setup Redis server
* [x] R1-0.7: Add Redis server as a task broker
* [x] R0.5-0.5: Create the needed environment variables to store the Django/Redis credentials
* [x] R1-0.5: Test manually Poly behaviors to find bugs that need to be solved
* [] R2: Fix the relevant bug that is causing the current frontend issue. 

> Frontend tasks (Chameleon)

* [x] R1-0.5: Convert numerical room id to an alphanumeric hash
* [x] R0.5-0.1: Connect the Room component to [qary.ai](https://www.qary.ai/) or [staging.qary.ai](https://staging.qary.ai/)
* [x] R0.5-0.5: Pop up the welcome message from the Web Socket server
* [x] R1-1: Fix the welcome message display on the Room component
* [x] R0.5-0.2: Add Triggers (options) to the Room component as buttons 
* [x] R1-0.2: Identify Triggers in the Room component
* [x] R1-0.5: Maintain the state by storing/updating the current state
* [x] R0.5-0.2: Have the user responses appear in the chat
* [x] R1-0.5: Normalize the user inputs to handle different formats (lowercase, etc)
* [x] R1-0.5: Submit user inputs by hitting Enter
* [x] R1-0.7: Fix the web socket connection issue when the welcome popup is off
* [x] R0.5-0.5: Prevent the web socket to be called at "Resume the ..." button
* [x] R1-0.7: Prevent the web socket to be called when exit/back to the conversation
* [x] R1-0.5: Edit the source to call the Web Socket within the Door component
* [] R1: Add 'Chameleon is typing ...' before the message loads

## Backlog

* [] R2: Continue learning and practicing Django channels/Web Sockets in the [learning repo](https://gitlab.com/rochdikhalid/django-channels)
* [] R2: Learn the basics of Python Celery and how it works
* [] R2: Practice Python Celery in the learning repo
* [] R?: Fix me: If user sends wrong response bot can't continue 
* [] R?: Fix me: Recognize variations on Espanol etc (nlp)
* [] R?: Fix me: Convert the has to a cryptogrpahic hash that includes a salt (key) for greater security
* [] Create release notes for 0.0.01 and 0.0.2
* [x] R1-0.1: Fix me: [#3](https://gitlab.com/tangibleai/nudger/-/issues/3) Rebrand the Nudger app
* [x] R2-1: Fix me: [#4](https://gitlab.com/tangibleai/nudger/-/issues/4) Add qary logo to home page at `qary.ai`
* [] R?: Fix me: [#5](https://gitlab.com/tangibleai/nudger/-/issues/5) Improve the Celery queueing / execution of messages
* [] Add a global trigger to reset conversation (copy/paste to all states - target: welcome state, reset - content dictionary)
* [] Create dialgoe for botfeedbac (add to existing YAML)
* [] Add security around eval function (Getattr instead)
* [] Be able to specify the variable name in the context dictionary from the YAML file (save whatever the user says into the variable in contenxt)
* [] Understand the sequence of events for logging/sending state (consumers.py > tasks.py).  Async the I/O bound tasks, and have them happen in the right order.


## Done: Sprint 2 (Aug 12 - Aug 19)

* [x] R0.5-0.2: Merge the new accomplished changes of the last sprint into `main`
* [x] R0.2-0.2: Make descriptive names for the variables x, y, and z
* [x] R0.1-0.1: Create a new file called `settings.json`
* [x] R0.1-0.1: Add the first configurables to the `settings.json` file
* [x] R0.2-0.2: Install and integrate `@rollup/plugin-json` plugin in the app
> The tasks listed below can be divided into additional tasks
* [x] R3-2: Refactor/restucture the source code to make it easy to configure
* [x] R2-1.5: Create a configurable that turn on/off the greeting component
* [x] R2-1: Create a configurable that allow/prevent empty messages to be sent
* [x] R1-0.5: Install and run Nudger locally
* [X] R0.2-R0.2: Create a Svelte draft component within **faceofqary** project
* [x] R2-R1: Connect the draft component to the `nudger`'s web socket server
* [x] R1-0.5: Pass a fetch to get the the welcome message
* [x] R1-1: Make another call that responds to **English**
* [x] R1-1.5: Store the web socket response and update the current state 