# faceofqary

Svelte chat bubble widget.

## Project Plan

Build a prototype Svelte chatbot widget

Goal is to port the popout chatbot widget called Poly to Svelte. 
The typescript implementation is here: https://wespeaknyc.cityofnewyork.us/  (parrot icon in lower right)

https://gitlab.com/tangibleai/moia-poly-chatbot (source code of the chatbot widget)

A similar Django app (with CSS and HTML to get you started) is here:
https://www.qary.ai/poly/

And a Svelte app that talks to a Django REST API is available here:
https://playground.proai.org/

We have front-end and back-end source code for all of these apps that we will share with you.

And of course you can get started at https://svelte.dev/repl
